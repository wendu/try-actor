open Actors
open Actorx.Util
open Actorx.Core
module Call = Actorx.Core.Node.Call
module Template = Actorx.Template

let _format_string = Printf.sprintf

type actor_reference = Call.actor_reference
type instance_reference = Call.instance_reference

let constructor_A : 'a Template.constructor =
  let tid = 0 in
  let local (ref : instance_reference) =
    object%actor (_)
      val ref : actor_reference = { tid; instance = ref }

      method ping (x : string) =
        print_endline "run ping";
        " ping" ^ x

      method pang a =
        print_endline "run pang";
        let p = a #! ping " pang" in
        let res = Promise.get p in
        "pang" ^ res

      method __ref = ref
    end
  and mock channel (ref : instance_reference) =
    object%actor (_)
      (* to talk to proxy for remote execution *)
      val local_channel = channel
      val ref : actor_reference = { tid; instance = ref }

      method ping (x : string) =
        let iref = ref.instance in
        Log.trace [] "run mock ping, nid=%d aid=%d" iref.nid iref.iid;
        let p = Proxy.Mock.async_at local_channel iref.nid iref.iid "ping" (Value x) in
        let res = Promise.await p in
        print_endline "recv mock ping";
        res

      method pang a =
        let iref = ref.instance in
        let arg_ref = a#.__ref in
        let p =
          Proxy.Mock.async_at local_channel iref.nid iref.iid "pang" (Actor arg_ref)
        in
        Promise.await p

      method __ref = ref
    end
  in
  { local; mock; tid }
;;

let local_run_A a mid (arg : Call.argument) ref_to_instance =
  match mid with
  | "ping" ->
    let arg' = Call.get_value arg in
    a #! ping arg'
  | "pang" ->
    let ref = Call.get_actor_ref arg in
    let instance = ref_to_instance ref in
    a #! pang instance
  | _ -> Dev.todo "handle other case"
;;

let app_main (env : 'a env) =
  Actors.Actor.Main.run
  @@ fun () ->
  Log.trace [ "master" ] "run master mode";
  let nodes = env.nodes in
  let n1 = nodes.(0)
  and n2 = nodes.(1)
  and n3 = nodes.(2) in
  let a1 = Actorx.Template.create env n1 constructor_A in
  let a2 = Actorx.Template.create env n2 constructor_A in
  let a3 = Actorx.Template.create env n3 constructor_A in
  let res1 = Promise.await (a2 #! pang a3) in
  print_endline @@ "result1:" ^ res1;
  let res2 = Promise.await (a2 #! pang a1) in
  print_endline @@ "result2:" ^ res2
;;

module Constructor = Actorx.Core.Node.Constructor

let constructors =
  let c = Constructor.Map.create () in
  Constructor.Map.add c constructor_A;
  c
;;

let () = Actorx.Main.run app_main constructors local_run_A
