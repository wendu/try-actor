# Try OCaml Actors

Distributed extension of the repo
https://gitlab.inria.fr/mandrieu/actors-ocaml

locally install dependency by opam pin add

## Run

master node: `dune exec main master [ip] [port]`

worker node: `dune exec main worker [server_ip] [server_port] [master_ip] [master_port]`


## Development
all method prefix by `_` is an internal function, which should be hidden by its `.mli`
