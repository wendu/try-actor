open Actorx__boot

let run app_main __constructors __runner =
  Arg.parse
    Sys.argv
    (Master.run app_main __constructors __runner)
    (Worker.run __constructors __runner)
;;
