open Eio
open Actorx__node
module Log = Actorx__util.Log
module Proxy = Network
open Message

module Master = struct
  let register (env : 'a Env.t) (counter : Counter.t) flow =
    let (worker_server : Address.t) = Network.recv flow in
    let id = Counter.peek counter in
    Env.add_server env id worker_server;
    Network.send flow id;
    Counter.increase counter;
    Counter.wait_all counter;
    Network.send flow env.servers
  ;;

  let _dispatch_call (env : 'a Env.t) worker_counter flow addr =
    match env.state with
    | Running -> Proxy.dispatch_call env flow addr
    | Setup -> register env worker_counter flow
  ;;

  let serve node_env eio_net server_addr worker_counter () =
    Switch.run
    @@ fun sw ->
    let addr = Address.to_eio server_addr in
    let socket = Net.listen ~reuse_port:true ~sw ~backlog:5 eio_net addr in
    Log.trace [ "master server" ] "Setup server";
    let _server =
      Net.run_server ~on_error:raise socket (_dispatch_call node_env worker_counter)
    in
    print_endline "master server finished"
  ;;

  (** Run proxy thread with network and local fibers *)
  let run server_addr node_env worker_counter =
    Eio_main.run
    @@ fun eio_env ->
    let net = Stdenv.net eio_env in
    Fiber.both (serve node_env net server_addr worker_counter) (fun _ ->
      Local.loop net node_env)
  ;;
end

module Worker = struct
  let serve node_env eio_net server_addr () =
    Switch.run
    @@ fun sw ->
    let addr = Address.to_eio server_addr in
    let socket = Net.listen ~reuse_port:true ~sw ~backlog:5 eio_net addr in
    Log.trace [ "worker server" ] "Setup";
    ignore @@ Net.run_server ~on_error:raise socket (Proxy.dispatch_call node_env)
  ;;

  let run server_addr node_env =
    Eio_main.run
    @@ fun eio_env ->
    let net = Stdenv.net eio_env in
    Fiber.both (serve node_env net server_addr) (fun _ -> Local.loop net node_env)
  ;;
end
