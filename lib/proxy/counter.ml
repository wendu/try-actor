(* Note: not thread safe! *)

open Eio

type t =
  { mutable n : int
  ; max : int
  ; changed : Condition.t
  }

let make max = { n = 1; max; changed = Condition.create () }

let wait_all t =
  while t.n <> t.max do
    Condition.await_no_mutex t.changed
  done
;;

let peek t = t.n

let increase t =
  if t.n >= t.max then failwith "more clients than expected" else t.n <- t.n + 1;
  Condition.broadcast t.changed
;;
