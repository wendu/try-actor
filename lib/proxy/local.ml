open Eio
open Actorx__node
open Actorx__util
module Promise = Actors.Promise

let counter = Counter.zero_counter ()
let trace = Log.trace

(** Transform a local async_at message to a network async_at.

    Give this call an ID for future reference. *)
let local_to_network (env : 'a Env.t) (data : Protocal.Local.async_at) =
  let id = counter#consume in
  let c' : Protocal.Network.async_at =
    { call = data.call; cid = id; return_nid = env.id }
  in
  c'
;;

(** Ask a remote node to do some computation *)
let initiate_async_at (env : 'a Env.t) net (c : Protocal.Local.async_at) =
  let network_call = local_to_network env c in
  let cid = network_call.cid in
  Log.info [ "send_run" ] "making remote call to node %d" c.nid;
  Eio.Switch.run
  @@ fun sw ->
  let addr = Server_map.find env.servers c.nid in
  let flow = Eio.Net.connect ~sw net (Address.to_eio addr) in
  Log.success [ "send_run" ] "make connection";
  let msg = Protocal.Network.Async_at network_call in
  Message.Network.send flow msg;
  Log.success [ "send_run" ] "send network call";
  let ack = Message.Network.recv flow in
  Log.success [ "send_run" ] "recv ack";
  match ack with
  | true -> Call_map.register env.calls cid c.resolver
  | false -> Promise.fail c.resolver (Failure "call a remote actor")
;;

(** Tell a remote node a previous computation finished *)
let initiate_finish node_env net (c : Protocal.Local.finish) =
  Switch.run
  @@ fun sw ->
  Log.trace [ "send_finish" ] "trying connect to node %d" c.nid;
  let addr = Env.address_of node_env c.nid in
  let flow = Eio.Net.connect ~sw net addr in
  Log.success [ "send_finish" ] "create connection";
  Protocal.Network.(send flow (Finish { cid = c.cid; result = c.value }));
  Log.success [ "send_finish" ] "finish sent"
;;

(** Ask a remote node to create an actor *)
let initiate_spawn node_env net (param : Protocal.Local.spawn) =
  Switch.run
  @@ fun sw ->
  Log.trace [ "send_spawn" ] "trying connect to node %d" param.nid;
  let addr = Env.address_of node_env param.nid in
  let flow = Eio.Net.connect ~sw net addr in
  Log.success [ "send_spawn" ] "create connection";
  Protocal.Network.(send flow (Spawn { tid = param.tid }));
  Log.success [ "send_spawn" ] "finish sent";
  (* wait remote node to make a ID *)
  let rid = Message.Network.recv flow in
  let r = param.wait_id in
  (* send back id to the thread which wishes to create this actor *)
  Promise.resolve r rid
;;

let loop net (env : 'a Env.t) =
  let stream = env.local_channel in
  while true do
    let msg = Protocal.Local.recv stream in
    match msg with
    | Async_at param -> initiate_async_at env net param
    | Finish param -> initiate_finish env net param
    | Spawn param -> initiate_spawn env net param
  done
;;
