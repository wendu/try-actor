open Eio
open Actorx__node
open Actorx__util

let string_of_address adr =
  match adr with
  | `Tcp (host, port) -> Format.asprintf "%a:%d" Eio.Net.Ipaddr.pp host port
  | _ -> failwith "not expected address type"
;;

let resolve_call (env : 'a Env.t) (x : Protocal.Network.finish) =
  Log.success [ "handle finish" ] "receive result %s for call %d" x.result x.cid;
  Call_map.resolve env.calls x.cid x.result;
  Log.success [ "proxy_loop"; "resolve call" ] "resolve call %d" x.cid
;;

let enqueue_return_to (env : 'a Env.t) nid cid result =
  let msg = Protocal.Local.Finish { nid; cid : int; value = result } in
  Protocal.Local.send env.local_channel msg;
  Log.success [ "enqueue_return_to" ] "add Finish message to stream"
;;

let ref_to_instance (env : 'a Env.t) (ref : Call.actor_reference) =
  let nid = env.id in
  match nid = ref.instance.nid with
  | true -> Actor_map.find env.actors ref.instance.iid
  | false ->
    let constructor = Constructor.Map.get env.__constructors ref.tid in
    let mock_instance =
      constructor.mock
        env.local_channel
        { nid = ref.instance.nid; iid = ref.instance.iid }
    in
    mock_instance
;;

let launch_call (env : 'a Env.t) flow (c : Protocal.Network.async_at) =
  Log.info [ "network proxy"; "launch_call" ] "actor %d, method %s" c.call.aid c.call.mid;
  (* the aid in call must be a local actor *)
  let actor = Actor_map.find env.actors c.call.aid in
  let p : string Actors.Promise.t =
    env.__local_runner actor c.call.mid c.call.arg (ref_to_instance env)
  in
  Actors.Promise.add_callback p (enqueue_return_to env c.return_nid c.cid);
  Message.Network.send flow true
;;

let create_local_actor (env : 'a Env.t) flow ({ tid } : Protocal.Network.spawn) =
  let id = Env.actor_counter#consume in
  let constructor = Constructor.Map.get env.__constructors tid in
  let instance = constructor.local { nid = env.id; iid = id } in
  Actor_map.add env.actors id instance;
  Message.Network.send flow id
;;

let dispatch_call (env : 'a Env.t) flow addr =
  Log.info
    [ "network proxy"; "dispatch_call" ]
    "recv connection from %s"
    (string_of_address addr);
  let msg = Message.Network.recv flow in
  match (msg : Protocal.Network.t) with
  | Async_at c ->
    Log.trace [ "network proxy"; "dispatch_call" ] "Async_at";
    launch_call env flow c
  | Finish c ->
    Log.trace [ "network proxy"; "dispatch_call" ] "Finish";
    resolve_call env c
  | Spawn param ->
    Log.trace [ "network proxy"; "dispatch_call" ] "Spawn";
    create_local_actor env flow param
;;

let loop env net addr port =
  Switch.run
  @@ fun sw ->
  let addr = `Tcp (addr, port) in
  let socket = Net.listen ~sw ~backlog:100 net addr in
  Log.trace [ "network proxy"; "loop" ] "try to setup server";
  let _server = Net.run_server ~on_error:raise socket (dispatch_call env) in
  ()
;;
