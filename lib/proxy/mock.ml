open Actorx__node

(* Interface after syntax generation for a real actor *)
let async_at stream nid aid mid (arg : Call.argument) =
  let promise, resolver = Actors.Promise.create () in
  let call : Call.t = { aid; mid; arg } in
  let data : Protocal.Local.async_at = { call; nid; resolver } in
  Protocal.Local.send stream (Protocal.Local.Async_at data);
  promise
;;
