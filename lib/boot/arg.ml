let exit () = failwith "wrong argument"

let parse argv master_main worker_main =
  let argc = Array.length argv - 1 in
  let get i =
    if argc < i then
      let msg = Printf.sprintf "try to get %d, only have %d arg" i argc in
      failwith msg
    else argv.(i)
  in
  let mode = get 1 in
  match mode with
  | "master" ->
      let count = int_of_string (get 2) in
      let ip = get 3 and port = int_of_string (get 4) in
      let arg : Master.t = { count; server = { ip; port } } in
      master_main arg
  | "worker" ->
      let server_ip = get 2 in
      let server_port = int_of_string (get 3) in
      let master_ip = get 4 and master_port = int_of_string (get 5) in
      let arg : Worker.t =
        {
          server = { ip = server_ip; port = server_port };
          master = { ip = master_ip; port = master_port };
        }
      in
      worker_main arg
  | _ -> exit ()
