open Actorx__node
open Actorx__proxy
open Actors
module Domainx = Actorx__util.Domainx

type t =
  { count : int
  ; server : Address.t
  }

let run app_main __constructors __runner (arg : t) =
  Eio_main.run
  @@ fun _eio_env ->
  let worker_counter = Counter.make arg.count in
  let node_env = Env.create_master arg.server __constructors __runner in
  let proxy_thread () =
    Actor.Main.run @@ fun () -> Server.Master.run arg.server node_env worker_counter
  in
  let proxy_domain = Domainx.spawn proxy_thread in
  Counter.wait_all worker_counter;
  node_env.state <- Running;
  Env.print node_env;
  print_endline "readly !";
  app_main (Env.App.from_env node_env);
  Domain.join proxy_domain
;;
