open Actorx__node
open Actorx__proxy
open Actorx__util

type t =
  { server : Address.t
  ; master : Address.t
  }

let _recv_servers flow : Server_map.t = Message.Network.recv flow

let _checkin (env : 'a Env.t) (worker_server : Address.t) flow =
  Message.Network.send flow worker_server;
  let (id : int) = Message.Network.recv flow in
  Actorx__util.Log.info [ "worker check in" ] "I am %d" id;
  let servers = _recv_servers flow in
  env.id <- id;
  env.servers <- servers;
  Env.print env
;;

let run __constructors __runner (arg : t) =
  let node_env = Env.create_worker __constructors __runner in
  let proxy_thread () =
    Actors.Actor.Main.run @@ fun () -> Server.Worker.run arg.server node_env
  in
  let proxy_domain = Domainx.spawn proxy_thread in
  Eio_main.run
  @@ fun env ->
  let net = Eio.Stdenv.net env in
  Eio.Switch.run
  @@ fun sw ->
  let addr = Address.to_eio arg.master in
  let flow = Eio.Net.connect ~sw net addr in
  Log.success [ "worker main" ] "connected to master";
  _checkin node_env arg.server flow;
  print_endline "readly !";
  Domain.join proxy_domain
;;
