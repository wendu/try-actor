open Core.Node
module Promise = Actors.Promise

type 'a constructor = 'a Constructor.t

let create (env : 'a Env.App.t) (target : Env.node) (constructor : 'a constructor) =
  let iid = Env.actor_counter#consume in
  let node_env = env.node in
  let instance =
    match target = node_env.id with
    | true -> constructor.local { nid = node_env.id; iid }
    | false ->
      (* make remote node to create its local actor *)
      let stream = node_env.local_channel in
      let p, r = Promise.create () in
      Protocal.Local.(
        send stream (Spawn { nid = target; tid = constructor.tid; wait_id = r }));
      (* wait id of that actor *)
      let rid = Promise.await p in
      let mock_instance =
        constructor.mock env.node.local_channel { nid = target; iid = rid }
      in
      mock_instance
  in
  Actor_map.add node_env.actors iid instance;
  instance
;;
