open Eio

module Network = struct
  let send flow value =
    Eio.Buf_write.with_flow flow
    @@ fun writer ->
    let data = Marshal.to_bytes value [] in
    let size = Bytes.length data in
    Buf_write.uint8 writer size;
    Buf_write.bytes writer data
  ;;

  let recv flow =
    let reader = Eio.Buf_read.of_flow flow ~max_size:100000 in
    let size = Buf_read.uint8 reader in
    let parser = Buf_read.take size in
    let data = parser reader in
    let offset = 0 in
    let value = Marshal.from_string data offset in
    value
  ;;
end
