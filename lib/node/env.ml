module IntMap = Hashtbl.Make (Int)

type node = int

type state =
  | Setup
  | Running

type role =
  | Master
  | Worker

type 'a actor = 'a Actor_map.actor

(* Workaround with the name resolution bug,
   Any code using actor_ppx will pollute namespace
*)

type local_channel = Protocal.Local.channel
type 'a local_constructor = int -> int -> 'a actor
type 'a mock_constructor = local_channel -> int -> int -> 'a actor

type 'a runner =
  'a actor
  -> string
  -> Call.argument
  -> (Call.actor_reference -> 'a actor)
  -> string Actors.Promise.t

(* end of workaround *)

type 'a t =
  { local_channel : local_channel
  ; calls : Call_map.t
  ; mutable state : state
  ; role : role
  ; mutable servers : Server_map.t
  ; mutable id : int
  ; actors : 'a Actor_map.t
  ; __constructors : 'a Constructor.map
  ; __local_runner : 'a runner
  }

let _create role servers __constructors __local_runner =
  let stream = Eio.Stream.create max_int in
  let id =
    match role with
    | Master -> 0
    | Worker -> 1
  in
  { local_channel = stream
  ; calls = Call_map.create ()
  ; state = Setup
  ; role
  ; servers
  ; id
  ; actors = Actor_map.create ()
  ; __constructors
  ; __local_runner
  }
;;

let create_master server_address __constructors __local_runner : 'a t =
  _create Master (Server_map.master_create server_address) __constructors __local_runner
;;

let create_worker __constructors __local_runner : 'a t =
  _create Worker (Server_map.worker_create ()) __constructors __local_runner
;;

let actor_counter = Actorx__util.Counter.zero_counter ()
let add_server env id server = IntMap.add env.servers id server

let print (env : 'a t) =
  let lines = [] in
  let lines = Printf.sprintf "I am %d" env.id :: lines in
  let lines = lines @ Server_map.format env.servers in
  print_endline (String.concat "\n" lines)
;;

let address_of env nid = Address.to_eio (Server_map.find env.servers nid)

module App = struct
  type 'a node_env = 'a t

  type 'a t =
    { nodes : node array
    ; node : 'a node_env
    }

  module Arrayx = Actorx__util.Arrayx

  let from_env env =
    let nids = Arrayx.keys env.servers in
    Arrayx.sort Int.compare nids;
    { nodes = nids; node = env }
  ;;
end
