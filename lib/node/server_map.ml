type t = Address.t Int_hash.t

let format (tb : t) =
  let to_str k (v : Address.t) list =
    let str = Printf.sprintf "[%d]->[%s]" k (Address.to_str v) in
    str :: list
  in
  let entries = Int_hash.fold to_str tb [] in
  entries
;;

let init_size = 10
let master_id = 0

let master_create server_addr =
  let servers = Int_hash.create init_size in
  Int_hash.add servers master_id server_addr;
  servers
;;

let worker_create () = Int_hash.create init_size
let find servers id = Int_hash.find servers id
