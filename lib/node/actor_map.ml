type 'a actor = 'a Actors.Actor.t
type 'a t = 'a actor Int_hash.t

let create () = Int_hash.create 10
let find actors id = Int_hash.find actors id
let add actors = Int_hash.replace actors
