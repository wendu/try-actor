type t = { ip : string; port : int }

let to_eio address =
  try
    let host = Unix.gethostbyname address.ip in
    let ip = host.h_addr_list.(0) in
    let ip = Eio_unix.Net.Ipaddr.of_unix ip in
    `Tcp (ip, address.port)
  with _ -> failwith "invalid endpoint"

let to_str addr = Printf.sprintf "%s:%d" addr.ip addr.port
