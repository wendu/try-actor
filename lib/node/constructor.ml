type 'a actor = 'a Actor_map.actor
type 'a local = Call.instance_reference -> 'a actor
type 'a mock = Protocal.Local.channel -> Call.instance_reference -> 'a actor

type 'a t =
  { local : 'a local
  ; mock : 'a mock
  ; tid : int
  }

type 'a map = 'a t Int_hash.t

module Map = struct
  let create () : 'a map = Int_hash.create 10
  let add map c = Int_hash.replace map c.tid c
  let get map id = Int_hash.find map id
end
