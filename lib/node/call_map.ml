type t = string Actors.Promise.resolver Int_hash.t

let create () = Int_hash.create 10

let resolve calls id result =
  let resolver = Int_hash.find calls id in
  Actors.Promise.resolve resolver result
;;

let register calls id resolver = Int_hash.add calls id resolver
