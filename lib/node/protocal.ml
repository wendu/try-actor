module Local = struct
  (** Message transfered from mock actor to proxy thread *)
  type async_at =
    { call : Call.t
    ; nid : int
    ; resolver : string Actors.Promise.resolver
    }

  (** Callback message by proxy to himself when handle a run *)
  type finish =
    { nid : int
    ; cid : int
    ; value : string
    }

  type spawn =
    { nid : int
    ; tid : int
    ; wait_id : int Actors.Promise.resolver
    }

  (** Messages that may appears in proxy thread from other threads *)
  type t =
    | Async_at of async_at (** actor ask proxy to make a call in another node *)
    | Finish of finish
    (** Actor finish a task that previous demanded by proxy,
        this message is actually added by callback to that actor call *)
    | Spawn of spawn

  type channel = t Eio.Stream.t

  let send (stream : channel) (value : t) = Eio.Stream.add stream value
  let recv (stream : channel) : t = Eio.Stream.take stream
end

module Network = struct
  (** Message received from network to run a call locally *)
  type async_at =
    { call : Call.t
    ; cid : int
    ; return_nid : int
    }

  (** Message received from network that a previous computation has finished *)
  type finish =
    { cid : int
    ; result : string
    }

  type spawn = { tid : int }

  (** Messages that may appears in proxy thread from network *)
  type t =
    | Async_at of async_at (** proxy asks a remote proxy to run a call *)
    | Finish of finish (** proxy tells a remote proxy there is a work finished *)
    | Spawn of spawn

  (* type channel = [ Eio.Flow.sink_ty | Eio.Flow.source_ty ] Eio.Resource.t *)

  let send flow (value : t) = Message.Network.send flow value
  let recv flow : t = Message.Network.recv flow
end
