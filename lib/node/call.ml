type instance_reference =
  { nid : int
  (** physical location of actor instance.
      For a local actor, this equals to env.id.
      For a mock actor this is the node ID where reside the corresponding local actor *)
  ; iid : int
  (** identifier of actor instance in a node.
      For a local actor, this is the key in env.actors.
      For a mock actor, this is the key to find its local instance in the remote node.*)
  }

type actor_reference =
  { tid : int
  (** type id, to identifier which kind of this actor, like a pointer to a class descripter.
      TODO Currently, this value is always 0 *)
  ; instance : instance_reference
  }

type argument =
  | Value of string
  | Actor of actor_reference

let get_value arg =
  match arg with
  | Value x -> x
  | Actor _ -> failwith "expecting a value but get a actor reference"
;;

let get_actor_ref arg =
  match arg with
  | Value _ -> failwith "expecting a actor reference but get a value"
  | Actor ref -> ref
;;

type t =
  { aid : int
  ; mid : string
  ; arg : argument
  }

let to_string (c : t) =
  match c.arg with
  | Value x -> Printf.sprintf "[%d][%s][%s]" c.aid c.mid x
  | _ -> failwith "can not string of call with actor referce"
;;
