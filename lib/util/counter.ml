class counter init =
  object (_self)
    val mutable n : int = init
    method peek = n

    method consume =
      let res = n in
      n <- n + 1;
      res
  end

let zero_counter () = new counter 0
