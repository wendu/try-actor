let trace prefixes fmt =
  let prefixes = List.map (fun x -> "[" ^ x ^ "]") prefixes in
  let prefix = String.concat "" prefixes in
  Printf.ksprintf
    (fun str ->
      Printf.printf "[%s  ] - %s - %s\n" (Color.detail "TRACE") prefix str;
      Stdlib.flush stdout)
    fmt

let todo prefixes fmt =
  let prefixes = List.map (fun x -> "[" ^ x ^ "]") prefixes in
  let prefix = String.concat "" prefixes in
  Printf.ksprintf
    (fun str ->
      Printf.printf "[%s   ] - %s - %s\n" (Color.danger "TODO") prefix str;
      Stdlib.flush stdout)
    fmt

let error prefixes fmt =
  let prefixes = List.map (fun x -> "[" ^ x ^ "]") prefixes in
  let prefix = String.concat "" prefixes in
  Printf.ksprintf
    (fun str ->
      Printf.printf "[%s  ] - %s - %s\n" (Color.danger "ERROR") prefix str;
      Stdlib.flush stdout)
    fmt

let success prefixes fmt =
  let prefixes = List.map (fun x -> "[" ^ x ^ "]") prefixes in
  let prefix = String.concat "" prefixes in
  Printf.ksprintf
    (fun str ->
      Printf.printf "[%s] - %s - %s\n" (Color.success "SUCCESS") prefix str;
      Stdlib.flush stdout)
    fmt

let info prefixes fmt =
  let prefixes = List.map (fun x -> "[" ^ x ^ "]") prefixes in
  let prefix = String.concat "" prefixes in
  Printf.ksprintf
    (fun str ->
      Printf.printf "[%s   ] - %s - %s\n" (Color.info "INFO") prefix str;
      Stdlib.flush stdout)
    fmt

let check expect real fmt =
  match expect = real with
  | true -> success [ "check" ] fmt
  | false -> error [ "check" ] fmt
