include Domain

(** Same as Domain.spawn but add a simple exception handler on top *)
let spawn f =
  spawn (fun () ->
    try f () with
    | e -> Log.error [ "domain" ] "%s" (Printexc.to_string e))
;;
