include Array
module IntHash = Hashtbl.Make (Int)

let keys tb =
  let keys_list = IntHash.fold (fun key _ acc -> key :: acc) tb [] in
  Array.of_list keys_list
