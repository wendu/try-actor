module Node = Actorx__node
module Proxy = Actorx__proxy

type 'a env = 'a Node.Env.App.t
