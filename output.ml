let person =
  Actors.Actor.Actor
    (object (_self)
       val $actor_scheduler_and_domain = lazy (Actors.Actor.spawn ())
       method scheduler = fst (Lazy.force ($actor_scheduler_and_domain))
       method domain = snd (Lazy.force ($actor_scheduler_and_domain))
       method ($actor_meth_hello) () =
         let _self = Actors.Actor.Actor _self in print_endline "hello"
       method hello =
         let _self = Actors.Actor.Actor _self in
         fun var_0 ->
           let (p, fill) = Actors.Promise.create () in
           Actors.Actor.send _self
             (Actors.Multiroundrobin.process fill
                (fun _ ->
                   (Actors.Actor.methods _self)#$actor_meth_hello var_0));
           p
       method hello_sync =
         let _self = Actors.Actor.Actor _self in
         fun var_0 ->
           if Actors.Actor.in_same_domain _self
           then (Actors.Actor.methods _self)#$actor_meth_hello var_0
           else Actors.Promise.get ((Actors.Actor.methods _self)#hello var_0)
       method hello_coop =
         let _self = Actors.Actor.Actor _self in
         fun var_0 ->
           if Actors.Actor.in_same_domain _self
           then (Actors.Actor.methods _self)#$actor_meth_hello var_0
           else
             Actors.Promise.await ((Actors.Actor.methods _self)#hello var_0)
       method hello_forward =
         let _self = Actors.Actor.Actor _self in
         fun var_0 ->
           Effect.perform
             (Actors.Multiroundrobin.Forward
                (fun forward ->
                   Actors.Actor.send _self
                     (Actors.Multiroundrobin.process forward
                        (fun _ ->
                           (Actors.Actor.methods _self)#$actor_meth_hello
                             var_0))))
     end)